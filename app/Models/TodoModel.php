<?php namespace App\Models;

use CodeIgniter\Model;


class TodoModel extends Model{
    
    
    protected $table='task';

    protected $allowedFields=['title','description','user_id'];

    public function getTodos(){
        $this->table('task');
        $this->select('title,description,firstname,lastname,task.id AS id');
        $this->join('user','user.id=task.user_id');
        $query=$this->get();

        return $query->getResultArray();
        
        //return $this->findAll();//findAll return all rows.
        }
        public function remove($id){
            $this->where('id',$id);
            $this->delete();
        }
}

class TodoModel extends Model{
    protected $table="user";
    protected $allowedFields =['username','password','firstname','lastname'];

    public function check($username,$password){
        $this->where('username',$username);
        $query=$this->get();
        $row=$query->getRow();
        if(password_verify($password,$row->password)){
            return $row;
        }
    
    return null;
}
}


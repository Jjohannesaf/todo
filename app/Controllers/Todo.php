<?php namespace App\Controllers;

use App\Models\LoginModel;

    const REGISTER_TITLE='Todo-Register';

class Login extends Basecontroller{
    public function index(){
        $data['title']='todo-Login';
        echo view ('templates/header',$data);
        echo view ('login/login',$data);
        echo view ('templates/footer',$data);
    }
    public function register(){
        $data['title']=REGISTER_TITLE;
        echo view ('templates/header',$data);
        echo view ('register/register');
        echo view('templates/footer');
    }
    public function registration(){
        $model=new LoginModel();

        if(!$this->validate([
            'user'=>'required|min_length[8]|max_length[30]',
            'password'=>'required|min_length[8]|max_length[30]',
            'confirmpassword'=>'required|min_length[8]|max_length[30]|matches[password]',
        ])){
            echo view ('templates/header',['title'=>REGISTER_TITLE]);
            echo view ('login/register');
            echo view('templates/footer');

        }
        else{
            $model->save([
                'username'=>$this->request->getVar('password'),PASSWORD_DEFAULT,
                'password'=>password_hash($this->request->getVar('password'),PASSWORD_DEFAULT),
                'firstname'=>$this->request->getVar('fname'),
                'lastname'=>$this->request->getVar('lname')
            ]);
            return redirect('login');
        }
    }
   
}
        
use App\Models\TodoModel;

class Login extends Basecontroller{
    
    public function index(){
        $data['title']='Todo-Login';
        echo view('templates/header',$data);//Set view and pass data
        echo view('login/login',$data);
        echo view ('templates/footer',$data);
    }
}

class Todo extends Basecontroller{

    /**
     * Constructor starts session.
     */

     public function __construct(){
         $session=\Config\Services::session();
         $session->start();
     }
    public function index(){
        $model=new TodoModel();
        $data['title']='Todo';
        $data['todos']=$model->getTodos();
        echo view ('templates/header',$data);
        echo view ('todo/list',$data);
        echo view('templates/footer',$data);
    }
    public function create(){
        $model=new TodoModel();

        if(!$this->validate([
            'title'=>'required|max_length[255]',
        ])){
            echo view('templates/header',['title'=>'Add new task']);
            echo view('todo(create');
            echo view('templates/footer');
        }
        else{
            $user=$_SESSION['user'];
            $model->save([
                'title'=>$this->request->getVar('title'),
                'description'=>$this->request->getVar('description'),
                'user_id'=>$user->id
                ]);
            return redirect('todo');
        
            }
    }

    public function check(){
        $model=new LoginModel();

        if(!this->validate([
            'user'=> 'required|min_length[8]|max_length[30]',
            'password'=>'required|min_length[8]|max_length[30]'
        ])){
            echo view ('templates/header',['title'=>LOGIN_TITLE]);
            echo view('login/login');
            echo view('templates/footer');
        }
        else{
            $user=$model->check(
                $this->request->getVar('user'),
                $this->request->getVar('password')
            );
            if($user){
                $_SESSION['user']=$user;
                return redirect('todo');
            }
            else{
                return redirect('login');
            }
        }
    
  
    
            }
            public function delete($id){
                if(!is_numeric($id)){
                    throw new \Exception('Provided id is not an number.');
                }
                if(!isset($_SESSION['user'])){
                    return redirect('login');
                }
                $model=new TodoModel();
                $model->remove($id);
                return redirect('todo');
            }
        }
    



